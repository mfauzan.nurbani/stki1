<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;

class StemmController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    public function stemm(request $request)
    {
      $text = $request->inputtext;
      //lowercase
      $text = strtolower($text);
      //echo "kalimat awal <br>";
      $textdipisah =  (explode(".",$text));

      for($i=0;$i<count($textdipisah);$i++){
        $textpisahlagi = (explode(" ",$textdipisah[$i]));
        //echo "Kalimat ".$i." panjangnya = ".count($textpisahlagi)."<br>";
      }
      //echo "<br>";
      //angka hilang
      $text = preg_replace('/[,]+/', '', $text);
      //$str = preg_replace('/[^a-zA-Z0-9 ]/','',$str1);

      $tokenizerFactory  = new \Sastrawi\Tokenizer\TokenizerFactory();
      $tokenizer = $tokenizerFactory->createDefaultTokenizer();

      $tokens = $tokenizer->tokenize($text);

      $stopWords = new \voku\helper\StopWords();
      $docstop = preg_replace('/\b('.implode('|',$stopWords->getStopWordsFromLanguage('id')).')\b/','',$tokens);
      $finalToken = array_filter(preg_replace('/[^a-zA-Z0-9 ]/','',$docstop));

      $temp = implode(" ",$finalToken);
      //array masih ngulang
      $finalToken = explode(" ",$temp);
      $raw = implode(" ", $finalToken);
      //hilang pengulangan
      $string = array_unique(explode(" ", $raw));
      $string = explode(" ",implode(" ",$string));

      //echo "batas <br>";
      //echo implode(" ",$finalToken)."<br>";
      //echo "string <br>";
      //echo implode(" ",$string)."<br>";

      //hitung semua kata tanpa angka masih ngulang
      //echo "Banyak kata masih ngulang = ". count($finalToken)."<br>";
      //echo "Banyak kata tanpa ngulang = ". count($string)."<br>";
      $average = count($finalToken)/count($string);
      //echo "Average = ".$average."<br>";

      //var_dump($string);
      //pembentukan wordlist
      $wordlist = array();
      for ($i=0;$i<count($string);$i++){
        if(substr_count($raw, $string[$i])>=$average){
          //echo $string[$i]." terjadi = ".substr_count($raw, $string[$i])."<br>";
          //echo "Masuk Wordlist <br>";
          array_push($wordlist, $string[$i]);
        }
      }
      //print_r($wordlist)."<br>";
      //print_r (explode(".",implode(" ",$docstop)));

      //mulai kalimat
      $sentenceFreq = array();
      $kalimat = explode(".",implode(" ",$docstop));
      //echo count($kalimat)."<br>";
      for($i=0;$i<count($kalimat);$i++){
        //mengandung angka
        array_push($sentenceFreq, 0 );
        if(1 === preg_match('~[0-9]~', $kalimat[$i])){
          $sentenceFreq[$i] = $sentenceFreq[$i]+1;
        }
        //mengandung wordlist
        $kalimatExplode = explode(" ",$kalimat[$i]);
        for($j=0;$j<count($kalimatExplode);$j++){
          for($k=0;$k<count($wordlist);$k++){
            if($kalimatExplode[$j] === $wordlist[$k]){
              $sentenceFreq[$i] = $sentenceFreq[$i]+1;
              //echo "ada ".$kalimatExplode[$j]."<br>";
            }
          }
        }
        //echo "freq kalimat '".$kalimat[$i]."' = ".$sentenceFreq[$i]."<br>" ;
      }
      $averageLength = count($tokens)/count($kalimat);
      //echo "Average Length = ".$averageLength."<br>";

      //final score
      $totalFrequency = 0;
      //echo "kalimat awal <br>";
      $textdipisah =  (explode(".",$text));

      for($i=0;$i<count($textdipisah);$i++){
        $score = 1;
        $textpisahlagi = (explode(" ",$textdipisah[$i]));
        $score = $score*($averageLength/count($textpisahlagi));
        $totalFrequency = $totalFrequency+$score;
      }
      //echo "Score total = ".$totalFrequency."<br>";
      $averageFrequency = $totalFrequency/count($textdipisah);
      //echo "Average Frequency = ".$averageFrequency."<br>";
      //hasilnya
      $hasil = array();
      for($i=0;$i<count($textdipisah);$i++){
        if($sentenceFreq[$i]>=$averageFrequency){
          array_push($hasil,  $textdipisah[$i]);
          //echo $textdipisah[$i].".";
        }
      }

      $awal = $request->inputtext;
      $hasil = implode(".",$hasil);
      return view('result', compact('hasil','awal'));
    }


    //for non stopword


    //get freq per word
    //Not stop word then frequency=frequency+1.
    /*
    public function notstopwordfreq(request $input, request $stpword){
      $frequency = 0;
      $length = str_word_count($input);
      $stplength = str_word_count($stpword)
      $perinput = explode(" ", $input);
      $perstpword = explode(" ", $stpword);
      for ($i=0;$i<$length;$i++){
        for($j=0;$j<$stplength;$j++){
          if($perinput[i]!=$perstpword){
            $frequency = $frequency+1;
          }
        }
      }
      return $frequency;
    }*/

    //just an example
    public function search(request $request){
        echo "ok";
    }

}
